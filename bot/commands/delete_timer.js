const { SlashCommandBuilder } = require('@discordjs/builders');


module.exports = { data: new SlashCommandBuilder()
    .setName('delete_timer')
    .setDescription('Change the time it takes for a message to auto-delete')
	.addIntegerOption(option =>
		option.setName('hours')
			.setDescription('Hours')
	)
	.addIntegerOption(option =>
		option.setName('minutes')
			.setDescription('Minutes')
	)
	.addIntegerOption(option =>
		option.setName('seconds')
			.setDescription('Seconds')
	)}

