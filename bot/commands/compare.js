const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = { data: new SlashCommandBuilder()
    .setName('compare')
	.setDescription('Chooses a game!')
	.addMentionableOption(option =>
		option.setName('player_1')
			.setDescription('Mention a player')
			.setRequired(true))
	.addMentionableOption(option =>
        option.setName('player_2')
            .setDescription('Mention a player'))
	.addMentionableOption(option =>
        option.setName('player_3')
            .setDescription('Mention a player'))
	.addMentionableOption(option =>
        option.setName('player_4')
            .setDescription('Mention a player'))
	.addMentionableOption(option =>
        option.setName('player_5')
            .setDescription('Mention a player'))
	.addMentionableOption(option =>
        option.setName('player_6')
            .setDescription('Mention a player'))
	.addMentionableOption(option =>
        option.setName('player_7')
            .setDescription('Mention a player'))
	.addMentionableOption(option =>
        option.setName('player_8')
            .setDescription('Mention a player'))
	} 