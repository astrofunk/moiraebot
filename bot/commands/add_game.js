const { SlashCommandBuilder } = require("@discordjs/builders");

module.exports = {
  data: new SlashCommandBuilder()
    .setName("add_game")
    .setDescription("Add a custom game!")
    .addStringOption((option) =>
      option
        .setName("game_title")
        .setDescription("Game title")
        .setRequired(true)
    )
    .addStringOption((option) =>
      option.setName("image_url")
	  .setDescription("Cover image for game")
    ),
};
