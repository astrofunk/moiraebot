const { SlashCommandBuilder } = require('@discordjs/builders');


module.exports = { data: new SlashCommandBuilder()
    .setName('link')
    .setDescription('Link a Steam account!')
	.addStringOption(option =>
		option.setName('steam_url')
			.setDescription('Linking Steam URL')
			.setRequired(true))
		}
