const { Client, Collection } = require("discord.js");
const gameEmbed = require("./gameEmbed");
const listEmbed = require("./listEmbed");
const db = require("../db/database_interactions");
const steam = require("../steam/steam_access");
const fs = require("fs");
const path = require("path");
const toTitleCase = require("./lib/titlecase");
const generator = require("./lib/list_generator");
require("dotenv").config({
  path: path.resolve(__dirname, `../.env.${process.env.NODE_ENV}`),
});

const client = new Client({
  intents: ["GUILDS", "GUILD_MESSAGES"],
});

client.once("ready", () => {
  console.log(`Logged in as ${client.user.tag}!`);
  client.user.setActivity(`/game to find a game!`);
});

client.commands = new Collection();
const commandFiles = fs
  .readdirSync("./bot/commands")
  .filter((file) => file.endsWith(".js"));

for (const file of commandFiles) {
  const command = require(`./commands/${file}`);
  // Set a new item in the Collection
  // With the key as the command name and the value as the exported module
  client.commands.set(command.data.name, command);
}

client.on("interactionCreate", async (interaction) => {
  if (!interaction.isCommand()) return;
  await db.addGuild(interaction.guildId, interaction.guild.name);
  if (interaction.commandName === "link") {
    const url = interaction.options.get("steam_url");
    console.log(url);
    if (
      url.value.match(/(steamcommunity\.com\/id\/)\S{3,32}\/?/gm) ||
      url.value.match(/(steamcommunity\.com\/profiles\/)\S{3,32}\/?/gm)
    ) {
      const steamID = await steam.checkSteam64(url.value);
      db.appendUser(
        interaction.member.id,
        steamID,
        interaction.member.user.username
      )
        .then(
          interaction.reply({
            content: `${url.value} has been linked`,
            ephemeral: true,
          })
        )
        .catch(console.error);
    } else {
      interaction.reply({
        content: `${url.value} is not a valid Steam URL`,
        ephemeral: true,
      });
    }
  }
  if (interaction.commandName === "game") {
    // Get our list of players with interaction.options
    // Check the value of each user to get their discord id
    //Then check to see if they have an existing link in the database
    //If not interaction reply with a suggestion to link steam using the /link command
    const guildGames = await db.getCustomGames(interaction.guildId);
    //Randomness of Steam vs server games is (# of server games ^ 2)/(2 * # of server games ^ 2  + 50 )
    const ggLen = guildGames.length.toFixed(2);
    let customGameChance =
      Math.pow(ggLen, 1.4) / (2.0 * Math.pow(ggLen, 1.4) + 30.0);
    console.log(
      "Chance of getting a custom game: " +
        (customGameChance * 100).toFixed(2) +
        "%"
    );
    let isSteamGame = interaction.options.get("include_steam").value;
    let gameData = guildGames[Math.floor(Math.random() * ggLen)];
    if (isSteamGame) isSteamGame = Math.random() >= customGameChance;
    let privatedSteams = false;
    //--------------
    /* STEAM LOGIC */
    //--------------
    if (isSteamGame) {
      const listArray = await generator.generateListArray(interaction);
      if (listArray) {
        console.log(listArray);
        console.log(`Player Count for this message is: ${listArray.length}`);
        const chosenGame = await steam.selectGame(
          await steam.filterGameList(listArray),
          listArray.length
        );
        gameData = await steam.getGameData(chosenGame.appid);
        gameEmbed.url = `http://steamcommunity.com/app/${chosenGame.appid}`;
        gameEmbed.title = chosenGame.name;
        if (gameData) {
          if (gameData.header_image)
            gameEmbed.image.url = gameData.header_image;
          if (gameData.short_description)
            gameEmbed.description = gameData.short_description;
        }
      } else {
        privatedSteams = true;
      }
    }

    //--------------
    /* STEAM LOGIC END */
    //--------------
    //embed is gameEmbed in this file
    /*
      {
        title: "minecraft",
        image: {
          url: "https://upload.wikimedia.org/wikipedia/en/5/51/Minecraft_cover.png"
        }
      }
    */
    if (!isSteamGame) {
      gameEmbed.title = toTitleCase(gameData.title);
      gameEmbed.image.url = gameData.image.url;
      gameEmbed.description = "";
    }

    console.log(gameEmbed);
    let current_players = "";
    interaction.options.forEach((player) => {
      if (player.type === "MENTIONABLE")
        current_players += `<@${player.value}> `;
    });
    interaction
      .reply({
        content: `Here's Your Game: ${current_players}`,
        embeds: [gameEmbed],
      })
      .then(() => {
        setTimeout(() => interaction.deleteReply(), 60000);
      });
    if (interaction.options.get("include_steam").value && privatedSteams) {
      interaction.followUp({
        content: `Could not include Steam games in this pick, due to one or more privated Steam accounts being included`,
      });
    }
  }
  if (interaction.commandName === "add_game") {
    const game_title = interaction.options.get("game_title").value;
    console.log("Game Title to be added: " + toTitleCase(game_title));
    interaction.reply({
      content: `Your custom game ${game_title} has been added!`,
    });
    await db.addGuildCustomGame(
      interaction.guildId,
      game_title,
      interaction.options.get("image_url")?.value
    );
  }
  if (interaction.commandName === "compare") {
    let gamesString = "";
    if(interaction.options.size < 2)
      interaction.reply({
        content: 'Requires more than 1 user for comparison',
        ephemeral: true
      })
    const sharedList = generator
      .generateListArray(interaction)
      .then((listArray) =>
        listArray
          ? steam.filterGameList(listArray).then((filteredList) => steam.allSharedGames(filteredList))
          : Promise.reject("Privated Steam accounts included")
      )
      .catch(console.error);
    const list = await sharedList;
    if (list) {
      for (const game of list) {
        gamesString = gamesString.concat(`${game}\n`);
      }
      console.log(gamesString);
      if (gamesString.length) {
        listEmbed.description = gamesString.slice(0, 4096);
      }
      let current_players = "";
      interaction.options.forEach((player) => {
        if (player.type === "MENTIONABLE")
          current_players += `<@${player.value}> `;
      });
      if(!interaction.replied){
        interaction
        .reply({
          content: `Here's Your List of Shared Games: ${current_players}`,
          embeds: [listEmbed],
        })
        .then(() => {
          setTimeout(() => interaction.deleteReply(), 60000);
        });
      }
    }
  }
});

client.on("guildCreate", async (guild) => {
  await db.addGuild(guild.id, guild.name);
});

client.login(process.env.DISCORD_TOKEN);
