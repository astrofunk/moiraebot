function capitalizeFirst(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

module.exports = function toTitleCase(game) {
  let words = game.split(" ");
  let firstWord = capitalizeFirst(words.shift());
  let connectingWords = ["of", "and", "the", "but", "an", "a"];
  game = [firstWord];
  for (let word of words) {
    if (connectingWords.includes(word)) {
      game.push(word);
    } else {
      word = capitalizeFirst(word);
      game.push(word);
    }
  }
  return game.join(' ');
}
