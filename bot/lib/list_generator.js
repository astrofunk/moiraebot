
const db = require("../../db/database_interactions");
const steam = require("../../steam/steam_access");

async function fetchSteamIDs(interaction) {
  let playerSteams = [];
  let users_string = "";
  for await (const option of interaction.options) {
    const player = option[1];
    if (player.type === "BOOLEAN") continue;
    let discord_id = player.value;
    const user = await db.getUser(discord_id);
    console.log(`Steam ID for user ${player.user.username} is ${user}`);
    if (!user) {
      users_string += `<@${discord_id}> `;
      console.log("User string: " + users_string);
    } else {
      playerSteams.push(user);
    }
  }
  if (users_string.length) {
    interaction.reply({
      content: `Link Steam account for users ${users_string} by using the /link command followed by your Steam account URL\nIf you've already linked your account, ensure that your Steam account and games list is *NOT* privated. Otherwise we won't be able to compare your games 😢`,
    });
    return false;
  }
  return playerSteams;
}



async function generateListArray(interaction) {
  let game_lists = [];
  const steamIDs = await fetchSteamIDs(interaction); //['steamid1', 'steamid_N']
  if(!steamIDs){
      return;
  }
  for await (id of steamIDs) {
    const list = await steam.generateGameList(id);
    console.log(`Steam Generated list`, list);
    if (!list) {
      return false;
    } else {
      game_lists.push(list);
    }
  }
  return game_lists;
}

module.exports = { generateListArray }