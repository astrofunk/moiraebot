const path = require('path');
require('dotenv').config({path: path.resolve(__dirname, `../.env.${process.env.NODE_ENV}`)});
const {
    MongoClient
} = require('mongodb');
const { get } = require('lodash');
/*
 * Connection URI. Update <username>, <password>, and <your-cluster-url> to reflect your cluster.
 * See https://docs.mongodb.com/ecosystem/drivers/node/ for more details
 */

const uri = `mongodb+srv://db_user_1:${process.env.DB_KEY}@cluster0.vx8di.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`;
const client = new MongoClient(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
//Initiate the client
client.connect().catch(console.error)

async function addGuild(guild_id, name) {
    let guild_collection = client.db("MoiraeStorage").collection('Guilds');
    if( await guild_collection.findOne({"_id": `${guild_id}`}) )
        return;
    await guild_collection.updateOne(
        {"_id": `${guild_id}`},
        { $set: {
        "name": name,
        "custom_games": [],
        "delete_timer": 30000
        }},
        { upsert: true }).then(console.log(`${name} has been upserted into DB`)).catch(console.error);
}

async function appendUser(discord_id, steam64_id, discord_username)
    {

    let user_collection = client.db("MoiraeStorage").collection('Users');
    await user_collection.updateOne({'_id': discord_id}, {$set: {'steam_id': steam64_id, 'discord_username': discord_username, 'date_created': `${new Date().toString()}`} }, {upsert: true}).then(console.log).catch(console.error);
}

async function clearUserArray(guild_id)
{
    let guild_collection = client.db("MoiraeStorage").collection('Guilds');
    await guild_collection.updateOne({"guild_id": `${guild_id}`}, {
        $set: {members: []}
    }).then(console.log).catch(console.error)
}


async function getUser(discord_id){
    let user_collection = client.db("MoiraeStorage").collection('Users');
    const member = await user_collection.findOne({'_id': `${discord_id}`}).catch(console.error);
    if(member)
        return member.steam_id; 
    return;
}

async function addGame(gameData, isMultiplayer) {
    let games_collection = client.db("MoiraeStorage").collection('Games')
    await games_collection.updateOne(
        { _id : gameData.appid },
        { $set: { title: gameData.title, image: gameData.image, description: gameData.description, isMultiplayer : isMultiplayer }},
        { upsert : true }
    ).catch(console.error)
}

async function addGuildCustomGame(guild_id, title, image_url = 'https://cdn.discordapp.com/avatars/862105727665831936/ec7fcaaadd02aff87c1a1a27d5de15a1.png?size=256'){
    let guild_collection = client.db("MoiraeStorage").collection('Guilds');
    let title_string = title.toLowerCase();
    console.log(`${title_string} will be added to guild ${guild_id}`)
    await guild_collection.updateOne(
        {_id: guild_id},
        { $pull: {'custom_games': {"title": title_string}}}
        ).then(console.log("Game Title Removed" + title_string)).catch(console.error);
    await guild_collection.updateOne(
        {_id: guild_id},
        {$addToSet: { 'custom_games': {"title": title_string, "image": {"url": image_url}}}}
        ).then(console.log("Game Title Added" + title_string)).catch(console.error);
}
async function getCustomGames (guild_id){
    let guild_collection = client.db("MoiraeStorage").collection('Guilds');
    const guild = await guild_collection.findOne(
        {_id: guild_id}
    )
    return guild.custom_games;


}
module.exports = {
    addGuild,
    appendUser,
    clearUserArray,
    getUser,
    addGame,
    addGuildCustomGame,
    getCustomGames
}