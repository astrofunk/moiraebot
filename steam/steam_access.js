/*TODO:
	- A function that will get called if we need to convert a vanity steam ID to a steam64 I
	- Have a function to get a list of games from a User and store that in temporary pool
			We can cut down on our list of games immmediately from the first person


*/
const fetch = require('node-fetch');
const _ = require('lodash');
const { filter } = require('lodash');

function checkSteam64(steamURL) {
	let token = steamURL.split("/");
	if (steamURL.charAt(steamURL.length - 1) !== '/') {
		token = token.pop();
	} else {
		token = token[token.length - 2];
	}
	if (token.match(/(\d{17})/g)) {
		console.log("Proper Steam64 ID: " + token);
		return token;
	}
	return convertVanityID(token);
}

async function convertVanityID(steamID){
	//Convert a vanity token into a steam64 ID
	// Everything written below this line
	//steamID = Lenzera
	let steam64 = '';
	let url = `http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key=${process.env.STEAM_API_KEY}&vanityurl=${steamID}`
    const getSteam64 = fetch(url)
        .then (response => response.json())
        .then (data => { return data.response.steamid })
		.catch(console.error)
	//Everything written above this line
	steam64 = await getSteam64;
	return steam64;
}

async function checkGameMultiplayer(appID){
	const gameIsMultiplayer = fetch(`https://store.steampowered.com/api/appdetails/?appids=${appID}`)
				.then(res => res.json())
				.then(gameData => gameData[appID].data.categories.filter(cats => cats.id === 1).length).then(isMulti => {return isMulti}).catch(console.error)
	let checkGame = await gameIsMultiplayer;
	return checkGame;
}
async function generateGameList(steam64){
    const getGameList = fetch(`http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=${process.env.STEAM_API_KEY}&steamid=${steam64}&format=json&include_appinfo=true`)
    .then (response => response.json())
    .then (data => {return data})
    .catch(console.error)
    let gameList = await getGameList;
	if(!_.size(gameList.response)){
			return false;
	}
	return gameList;
}

async function filterGameList(lists){
	//console.log(lists[0].response.games);
	if(lists.length === 1){
		return lists[0].response.games;
	}
	let lookup = _.keyBy(lists[0].response.games, game => game.appid);
	let comparison = {};
	for (let i = 1; i < lists.length; i++) {
		comparison = _.filter(lists[i].response.games, game => lookup[game.appid] !== undefined)
		lookup = _.keyBy(comparison, game => game.appid);
	}
	console.log(comparison);
	return comparison;
}

async function allSharedGames(comparedList){
	const comparison = comparedList;
	let sharedGames = [];
	for(const game of comparison){
		sharedGames.push(game.name);
	}
	return sharedGames;
}


async function selectGame(games, playerCount){
	//console.log(games[_.random(games.length - 1)]);
	let chosen_game = games[_.random(games.length - 1)];
	console.log(chosen_game.name);
	if(!getGameData(chosen_game.appid))
		return selectGame(games);
	if(playerCount === 1)
		return chosen_game;
	let isMultiplayer = await checkGameMultiplayer(chosen_game.appid)
	if(isMultiplayer)
		return chosen_game
	else
		return selectGame(games);
}
async function getGameData(appid){
	const gameData = fetch(`https://store.steampowered.com/api/appdetails/?appids=${appid}`).then(res => res.json()).then(appdata => {return appdata[appid].data});
	return gameData;
}

module.exports = { checkSteam64, generateGameList, selectGame, getGameData, filterGameList, allSharedGames}