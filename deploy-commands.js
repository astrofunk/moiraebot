const { REST } = require("@discordjs/rest");
const { Routes } = require("discord-api-types/v9");
const { clientId, testId, guildId } = require("./config.json");
const fs = require("fs");
const path = require("path");
require("dotenv").config({
  path: path.resolve(__dirname, `.env.${process.env.NODE_ENV}`),
});

const commands = [];

const commandFiles = fs
  .readdirSync("./bot/commands")
  .filter((file) => file.endsWith(".js"));

for (const file of commandFiles) {
  const command = require(`./bot/commands/${file}`);
  console.log(command);
  commands.push(command.data.toJSON());
}
const rest = new REST({ version: "9" }).setToken(process.env.DISCORD_TOKEN);

(async () => {
  try {
    if (process.env.NODE_ENV === "dev") {
      await rest.put(Routes.applicationGuildCommands(testId, guildId), {
        body: commands,
      }).catch(console.error);
      console.log("Successfully registered guild application commands for test bot.");
    } else {
      await rest.put(Routes.applicationCommands(clientId), { body: commands });
      console.log("Successfully registered global application commands.");
    }
  } catch (error) {
    console.error(error);
  }
})();
